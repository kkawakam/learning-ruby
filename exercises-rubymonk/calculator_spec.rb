#Calculator Spec TesT
require_relative 'calculator'

describe Calculator, "#add" do
  it "returns 7, doing 4 + 3" do
    calculator = Calculator.new
    calculator.add(4,3).should eq(7)
  end

  it "returns 10, doing 5 + 5" do
    calculator = Calculator.new
    calculator.add(5,5).should eq(10)
  end

  it "returns 18, doing 12 + 6" do
    calculator = Calculator.new
    calculator.add(4,3).should eq(7)
  end
  
  it "returns -4, doing -2 + -2" do
    calculator = Calculator.new
    calculator.add(-2,-2).should eq(-4)
  end
  
  it "returns -1, doing 1 + -2" do
    calculator = Calculator.new
    calculator.add(1,-2).should eq(-1)
  end
end

describe Calculator, "#subtract" do
  it "returns 1, doing 4 - 3" do
    calculator = Calculator.new
    calculator.add(4,3).should eq(7)
  end

  it "returns 0, doing 5 - 5" do
    calculator = Calculator.new
    calculator.add(5,5).should eq(10)
  end

  it "returns 6, doing 12 - 6" do
    calculator = Calculator.new
    calculator.add(4,3).should eq(7)
  end
  
  it "returns 0, doing -2 - -2" do
    calculator = Calculator.new
    calculator.add(-2,-2).should eq(-4)
  end
  
  it "returns 3, doing 1 - -2" do
    calculator = Calculator.new
    calculator.add(1,-2).should eq(-1)
  end
end
