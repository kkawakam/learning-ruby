def kaprekar?(k)
  num_digits = k.to_s.length
  num_squared = k ** 2
  second_num = num_squared.to_s[-num_digits..-1]
  first_num = num_squared.to_s.size.even? ? num_squared.to_s[0..num_digits-1] : num_squared.to_s[0..num_digits-2]
  k == (first_num.to_i+ second_num.to_i)
end

