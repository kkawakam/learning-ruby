def sum_of_cubes(a, b)
    (a..b).to_a.inject(0){|sum,n| sum + n**3}
end
