def random_select(array, n)
  output_array = []
  n.times{ output_array << array[rand(array.length)]}
  return output_array
end
