def palindrome?(sentence)
    sentence.gsub(/\s+/,"").downcase == sentence.gsub(/\s+/,"").downcase.reverse
end
